﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{
    public List<VideoClip> videos = null;

    public VideoEvent onPause = new VideoEvent();
    public VideoEvent onLoad = new VideoEvent();

    

    private bool isPaused = false;
    public bool IsPaused
    {
        get
        {
            return isPaused;
        }

        private set
        {
            isPaused = value;
            onPause.Invoke(isPaused);
        }
    }

    private bool isVideoReady = false;
    public bool IsVideoReady
    {
        get
        {
            return isVideoReady;
        }

        private set
        {
            isVideoReady = value;
            onLoad.Invoke(isVideoReady);
        }
    }

    private int index = 0;
    private VideoPlayer videoPlayer = null;
    private Camera uiCamera = null;
    private Canvas uiCanvas = null;

    public static void DumpToConsole(object obj)
    {
        var output = JsonUtility.ToJson(obj, true);
        Debug.Log(output);
    }

    private void ShowUI()
    {
        //GUI.backgroundColor = Color.cyan;
        // uiCamera.enabled = true;
        uiCanvas.enabled = true;
        Debug.Log("camera show");
    }

    private void HideUI()
    {
        //  uiCamera.enabled = false;
        uiCanvas.enabled = false;
        Debug.Log("camera hide");
    }

    private void Awake()
    {
        videoPlayer = GetComponent<VideoPlayer>();
        videoPlayer.seekCompleted += OnComplete;
        videoPlayer.prepareCompleted += OnComplete;
        videoPlayer.loopPointReached += OnLoop;
        //uiCamera = GameObject.FindWithTag("UICamera").GetComponent<Camera>();
        //uiCamera.enabled = false;

        uiCanvas = GameObject.FindWithTag("UICanvas").GetComponent<Canvas>();
        uiCanvas.enabled = false;
        //Debug.Log("log cam");
        //var bork = Camera.allCameras;
        //foreach(Camera camera in bork)
        //DumpToConsole(uiCamera);

        //GetComponent<Button>().onClick.AddListener(GoToFrame);
        //hitA = GameObject.FindWithTag("HotSpot01").GetComponent<Button>();
    }

    private void Start()
    {
        StartPrepare(index);
    }

    public void PauseToggle()
    {
        IsPaused = !videoPlayer.isPaused;

        if (IsPaused)
        {
            ShowUI();
            videoPlayer.Pause();
        }
        else
        {
            HideUI();
            videoPlayer.Play();
        }
    }

    private void OnDestroy()
    {
        videoPlayer.seekCompleted += OnComplete;
        videoPlayer.prepareCompleted += OnComplete;
        videoPlayer.loopPointReached += OnLoop;
    }

    public void SeekForward()
    {
        StartSeek(10.0f);
    }

    public void SeekBack()
    {
        StartSeek(-10.0f);
    }

    private void StartSeek(float seekAmount)
    {
        IsVideoReady = false;
        videoPlayer.time += seekAmount;
        //videoPlayer.Play();
        uiCanvas.enabled = false;
    }

    public void NextVideo()
    {
        index++;

        if (index == videos.Count)
            index = 0;

        StartPrepare(index);
    }

    public void PreviousVideo()
    {
        index--;
        if (index == -1)
            index = videos.Count - 1;

        StartPrepare(index);
    }

    private void StartPrepare(int clipIndex)
    {
        IsVideoReady = false;
        videoPlayer.clip = videos[clipIndex];
        videoPlayer.Prepare();
        
    }

    private void OnComplete(VideoPlayer videoPlayer)
    {
        IsVideoReady = true;
        videoPlayer.Play();
    }

    private void OnLoop(VideoPlayer videoPlayer)
    {
        NextVideo();
    }

    public void GoToFrame(float value)
    {
        IsVideoReady = false;
        videoPlayer.time = value;
        uiCanvas.enabled = false;
    }
    [System.Serializable]
    public class VideoEvent : UnityEvent<bool> { }
}
