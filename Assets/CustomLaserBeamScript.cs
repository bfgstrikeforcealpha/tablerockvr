﻿
    using UnityEngine;

    public class CustomLaserBeamScript :MonoBehaviour
    {
        LaserPointer lp;
        LineRenderer lr;

        public LaserPointer.LaserBeamBehavior laserBeamBehavior;

        void Start()
        {
            lp = FindObjectOfType<LaserPointer>();
            lp.laserBeamBehavior = laserBeamBehavior;
    }
    }
