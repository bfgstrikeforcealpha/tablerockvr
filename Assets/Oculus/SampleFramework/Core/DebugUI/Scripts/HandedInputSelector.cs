/************************************************************************************

Copyright (c) Facebook Technologies, LLC and its affiliates. All rights reserved.  

See SampleFramework license.txt for license terms.  Unless required by applicable law 
or agreed to in writing, the sample code is provided �AS IS� WITHOUT WARRANTIES OR 
CONDITIONS OF ANY KIND, either express or implied.  See the license for specific 
language governing permissions and limitations under the license.

************************************************************************************/
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class HandedInputSelector : MonoBehaviour
{
    OVRCameraRig m_CameraRig;
    OVRInputModule m_InputModule;
    //OVRInputModule m_InputModuleR;

    public Transform AnchorTransformL;
    public Transform AnchorTransformR;
    private bool isRightHand = true;

    void Start()
    {
        m_CameraRig = FindObjectOfType<OVRCameraRig>();
        m_InputModule = FindObjectOfType<OVRInputModule>();
        //m_InputModuleR = FindObjectOfType<OVRInputModule>();
        SetActiveController(OVRInput.Controller.RTouch);
    }

    void Update()
    {
        if(isRightHand)
        if(OVRInput.GetDown(OVRInput.Button.Any,OVRInput.Controller.LTouch))
        //if(OVRInput.GetActiveController() == OVRInput.Controller.LTouch)
        {
            isRightHand = false;
            SetActiveController(OVRInput.Controller.LTouch);
        }

        if (!isRightHand)
            if (OVRInput.GetDown(OVRInput.Button.Any, OVRInput.Controller.RTouch))
                //if(OVRInput.GetActiveController() == OVRInput.Controller.LTouch)
            {
                isRightHand = true;
                SetActiveController(OVRInput.Controller.RTouch);
            }
     

    }

    void SetActiveController(OVRInput.Controller c)
    {
        Transform t;
        if(c == OVRInput.Controller.LTouch)
        {
            t = AnchorTransformL; // m_CameraRig.leftHandAnchor;
        }
        else
        {
            t = AnchorTransformR; // m_CameraRig.rightHandAnchor;
        }
        m_InputModule.rayTransform = t;
        //m_InputModuleR.rayTransform = t;
    }
}
